PORT="${PORT:-8080}"

newman run postman.collection  --reporters cli,html,junit --reporter-html-export report.html --reporter-junit-export report.xml &&

python -m uvicorn main:app --port $PORT --host 0.0.0.0 --forwarded-allow-ips '*' --reload