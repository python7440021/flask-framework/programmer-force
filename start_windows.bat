:: This method is not recommended, and we recommend you use the `start.sh` file with WSL instead.
@echo off
SETLOCAL ENABLEDELAYEDEXPANSION

:: Get the directory of the current script
SET "SCRIPT_DIR=%~dp0"
cd /d "%SCRIPT_DIR%" || exit /b

SET "PORT=%PORT:8080%"



:: Execute uvicorn

 uvicorn main:app --host 0.0.0.0 --port "%PORT%" --forwarded-allow-ips '*'
