
const newman = require('newman');


newman.run({
    collection: require('postman.collection'),
    reporters: ['cli', 'html']

}, function (err) {
    if (err) {


        throw err;
    }

    exports.err = (err) => err;

    console.log('collection run complete!');
});