# Assessment Task # 4

- [ ] Create an API in the Flask framework of Python and 
  - [ ] return a static response in JSON OR Use any Testing API endpoint. 
- [ ] 2\. On Testing API endpoint OR your own created endpoint 
  - [ ] create API testcases collection with all possible scenarios according to the response and environment
  - [ ] Some scenarios like validating the response time, headers and other cases
    - [ ] use newman to run test cases 
    - [ ] generate reports.
